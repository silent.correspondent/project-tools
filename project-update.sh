#!/bin/bash

source lib/utils.sh

usage() {
	error "Usage: <-p project-name> <-o output-dir>"
}

while getopts ":p:x:o:" opt; do
	case "${opt}" in
		p)
			projname=${OPTARG}
			;;
		o)
			outdir=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done

[ -z "${projname}" ] || [ -z "${outdir}" ] && usage

mkdir -p ${outdir}

gdrive=$(which gdrive-linux-x64)

items=$(${gdrive} list | grep ${projname})
for report in ${outdir}/*; do
	obj=$(basename ${report})
	id=$(echo "${items}" | grep ${obj} | cut -f1 -d" ")
	${gdrive} update ${id} ${report}
done

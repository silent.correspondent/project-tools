#! /bin/bash

# iterate through all the versions of the
# given file. The given callback is invoked
# for each version.
#
# The local directory is set to the root of the
# repo. Sets some local variables with the git_
# prefix usable by the callback
git-iter() {
	local callback=$1
	local file=$(realpath $2)
	local dir=$(dirname ${file})

	pushd ${dir} > /dev/null
	cd $(git rev-parse --show-toplevel ${file})
	local git_file=$(echo ${file} | sed -e "s|$(pwd)/||g")

	entries=$(git log --shortstat --reverse --date=short --format="%H,%cd" \
		| grep ^[0-9a-z])
	for e in ${entries}; do
		local git_sha1=$(echo $e | cut -f1 -d,)
		local git_date=$(echo $e | cut -f2 -d,)
		${callback}
	done
	popd > /dev/null
}

#!/bin/bash

source lib/utils.sh
source lib/data.sh

usage() {
	error "Usage: <-p project-name> <-x xml-source> <-o output-dir>"
}

while getopts ":p:x:o:" opt; do
	case "${opt}" in
		p)
			projname=${OPTARG}
			;;
		x)
			xml=${OPTARG}
			;;
		o)
			outdir=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done

[ -z "${projname}" ] || [ -z "${xml}" ] || [ -z "${outdir}" ] && usage

mkdir -p ${outdir}

# Query to list resolved issues
resolved=$(xmlstarlet sel -T -t -m TODOLIST/TASK[@PERCENTDONE="100"] -v \
	"concat(@ID, ',', CUSTOMATTRIB[@ID='CUST_CATEGORY']/@VALUE, ',', @TITLE)" \
	-n ${xml})

# Query to list pending issues
pending=$(xmlstarlet sel -T -t -m TODOLIST/TASK[@PERCENTDONE!="100"] -v \
	"concat(@ID, ',', CUSTOMATTRIB[@ID='CUST_CATEGORY']/@VALUE, ',', @TITLE)" \
	-n ${xml})

# Filter out records for given category and issues list
filter() {
	category=$2
	sorted_list="$(echo "$1" | sort -n)"
	echo "ID,Description"
	for row in "${sorted_list}"; do
		echo "${row}" | grep ".*,${category}" | sed "s/,${category}//g"
	done
}

for c in ${categories}; do
	prefix=${projname}-$(echo ${c}s | tr [A-Z] [a-z])
	filter "${resolved}" ${c} > ${outdir}/${prefix}-resolved.csv
	filter "${pending}" ${c} > ${outdir}/${prefix}-pending.csv
done

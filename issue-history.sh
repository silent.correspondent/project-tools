#!/bin/bash

source lib/utils.sh
source lib/data.sh
source lib/git.sh

usage() {
	error "Usage: <-p project-name> <-x xml-source> <-o output-dir>"
}

while getopts ":p:x:o:" opt; do
	case "${opt}" in
		p)
			projname=${OPTARG}
			;;
		x)
			xml=${OPTARG}
			;;
		o)
			outdir=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done

[ -z "${projname}" ] || [ -z "${xml}" ] || [ -z "${outdir}" ] && usage

mkdir -p ${outdir}

# Callback invoked for each iteration of the git history
# See git-iter
callback() {
	local old=/tmp/.xml
	git show ${git_sha1}:${git_file} > ${old}
	echo -n ${git_date},
	xmlstarlet sel -t -v "count(TODOLIST/TASK/CUSTOMATTRIB[@ID='CUST_CATEGORY' and @VALUE='$c'])" ${old}
	echo -n ,
	xmlstarlet sel -t -v "count(//TASK[@PERCENTDONE=100 and CUSTOMATTRIB[@ID='CUST_CATEGORY' and @VALUE='$c']])" ${old}
	echo
}

gen-reports() {
	for c in ${categories}; do
		file=${outdir}/${projname}-$(echo ${c} | tr [A-Z] [a-z])s-history.csv
		echo Date,Total,Resolved > ${file}
		git-iter callback ${xml} >> ${file}
	done
}

gen-reports

#!/bin/bash

source lib/utils.sh

usage() {
	error "Usage: <-p project-name> <-o output-dir>"
}

while getopts ":p:x:o:" opt; do
	case "${opt}" in
		p)
			projname=${OPTARG}
			;;
		o)
			outdir=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done

[ -z "${projname}" ] || [ -z "${outdir}" ] && usage

mkdir -p ${outdir}

gdrive=$(which gdrive-linux-x64)
project=$1

# Create the project folder if it doesn't already
# exist
folder=${projname}-reports
entry=$(${gdrive} list | grep ${folder})

if [[ -z ${entry} ]] ; then
	${gdrive} mkdir ${folder}
	entry=$(${gdrive} list | grep ${folder})
fi

folder_id=$(echo ${entry} | cut -f1 -d" ")

# Create empty reports

for report in ${outdir}/*; do
	file=$(basename ${report})
	entry=$(${gdrive} list | grep ${file})
	if [[ -z ${entry} ]] ; then
		${gdrive} upload --mime 'text/csv' ${report} -p ${folder_id}
	fi
done
echo Please convert uploaded files to sheets
